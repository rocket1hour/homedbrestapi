package imlv.home.entities;

import org.springframework.stereotype.Repository;

@Repository
public class Document {

    String documentId;
    Integer documentType;
    String producerName;
    Integer releaseNo;

    public Document() {
    }

    public Document(String documentId, Integer documentType, String producerName, Integer releaseNo) {
        this.documentId = documentId;
        this.documentType = documentType;
        this.producerName = producerName;
        this.releaseNo = releaseNo;
    }

    public String info() {
        return this.toString();
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public Integer getDocumentType() {
        return documentType;
    }

    public void setDocumentType(Integer documentType) {
        this.documentType = documentType;
    }

    public String getProducerName() {
        return producerName;
    }

    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }

    public Integer getVersionNo() {
        return releaseNo;
    }

    public void setVersionNo(Integer releaseNo) {
        this.releaseNo = releaseNo;
    }

    @Override
    public String toString() {
        return "{" + "documentId=" + documentId + ", documentType=" + documentType + ", producerName=" + producerName + ", releaseNo=" + releaseNo;
    }
    
    // @Repository
    public class Book extends Document{
        
        String authorName;
        String pageCount;

        public Book() {
        }
        
        public Book(String documentId, Integer documentType, String producerName, Integer releaseNo, String authorName, String pageCount) {
            super(documentId, documentType, producerName, releaseNo);
            this.authorName = authorName;
            this.pageCount = pageCount;
        }

        @Override
        public String info(){
            return super.info();
        }
        
        public String getAuthorName() {
            return authorName;
        }

        public void setAuthorName(String authorName) {
            this.authorName = authorName;
        }

        public String getPageCount() {
            return pageCount;
        }

        public void setPageCount(String pageCount) {
            this.pageCount = pageCount;
        }

        @Override
        public String toString() {
            return super.toString() + ", authorName=" + authorName + ", pageCount=" + pageCount + "}";
        }
    }
    
    // @Repository
    public class Magazine extends Document{
        Integer releaseVers;
        Integer releaseMonth;

        public Magazine() {
        }

        public Magazine(String documentId, Integer documentType, String producerName, Integer releaseNo, Integer releaseVers, Integer releaseMonth) {
            super(documentId, documentType, producerName, releaseNo);
            this.releaseVers = releaseVers;
            this.releaseMonth = releaseMonth;
        }

        @Override
        public String info() {
            return super.info();
        }

        public Integer getReleaseVers() {
            return releaseVers;
        }

        public void setReleaseVers(Integer releaseVers) {
            this.releaseVers = releaseVers;
        }

        public Integer getReleaseMonth() {
            return releaseMonth;
        }

        public void setReleaseMonth(Integer releaseMonth) {
            if (releaseMonth < 1) {
                this.releaseMonth = 1;
            } else if (releaseMonth > 12) {
                this.releaseMonth = 12;
            } else {
                this.releaseMonth = releaseMonth;

            }
        }        

        @Override
        public String toString() {
            return super.toString() + ", releaseVers=" + releaseVers + ", releaseMonth=" + releaseMonth + "}";
        }
    }
    
    // @Repository
    public class Newspaper extends Document{
        Integer releaseDate;

        public Newspaper() {
        }

        public Newspaper(String documentId, Integer documentType, String producerName, Integer releaseNo, Integer releaseDate) {
            super(documentId, documentType, producerName, releaseNo);
            this.releaseDate = releaseDate;
        }

        @Override
        public String info() {
            return super.info();
        }

        public Integer getReleaseDate() {
            return releaseDate;
        }

        public void setReleaseDate(Integer releaseDate) {
            if (releaseDate < 1) {
                this.releaseDate = 1;
            } else if (releaseDate > 31) {
                this.releaseDate = 31;
            } else {
                this.releaseDate = releaseDate;
            }
        }

        @Override
        public String toString() {
            return super.toString() + ", releaseDate=" + releaseDate + "}";
        }
    }
}
