package imlv.home.util;

import com.google.gson.JsonArray;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.Class;
import java.nio.charset.StandardCharsets;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Util {

    // private static final String PATH_CLASS_CHUYEN_XE = "home.oop.benxe.entity";
    // private static final String CHUYEN_XE_NOI_THANH = ".NoiThanh";
    // private static final String CHUYEN_XE_NGOAI_THANH = ".NgoaiThanh";

    public File getFileFromResources(String fileName) { 
        try {
            File file = new File(getClass().getClassLoader().getResource(fileName).toURI());
            return file;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public String getInputStreamFromResources(String fileName) { 
        try {
            // Read file json
            // InputStream dataInitStream = imlv.home.util.class.getResourceAsStream("/xes/xes.json");
            InputStream dataInitStream = Util.class.getResourceAsStream(fileName);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[1024];
            while ((nRead = dataInitStream.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            byte[] byteArray = buffer.toByteArray();
            String content = new String(byteArray, StandardCharsets.UTF_8);
            content = content.trim();
            return content;
        } catch (Exception e) {
            System.out.println(e);
        }
        return "";
    }

    public JsonObject convertStringToJsonObject(String fileName){
        String contentJson = getInputStreamFromResources(fileName);
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(contentJson);
        return jsonElement.getAsJsonObject();
    }

    public JsonArray convertStringToJsonArray(String fileName){
        String contentJson = getInputStreamFromResources(fileName);
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(contentJson);
        return jsonElement.getAsJsonArray();
    }
    
    public static Class<?> getUtil(){
        return new Util().getClass();
    }

    public static Util getNewUtil(){
        return new Util();
    }
}