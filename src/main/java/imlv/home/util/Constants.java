package imlv.home.util;

public class Constants {
    
//    Entity Info
    public static final String ENTIRY_INFO_ID = "documentId";
    
//    Entity Type
    public static final String BOOK_FIELD_ONLY = "book";
    public static final String MAGAZINE_FIELD_ONLY = "magazine";
    public static final String NEWSPAPER_FIELD_ONLY = "newspaper";
    
//    Type
    public static final String ENTITY_TYPE = "documentType";
    public static final Integer ALL_ENTITY_TYPE_DOCUMENT = 0;
    public static final Integer ENTITY_TYPE_BOOK = 1;
    public static final Integer ENTITY_TYPE_MAGAZINE = 2;
    public static final Integer ENTITY_TYPE_NEWSPAPER = 3;
    

}