package imlv.home.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import imlv.home.entities.Document;
import imlv.home.services.library.LibraryServices;

@Component
@RestController
@RequestMapping("/imlv/home/book")
public class SampleRestController {

    @Autowired
    @Qualifier("book")
    private LibraryServices libraryServices;

    @PostConstruct
    public void init(){
        
    }

    @GetMapping("/get-all")
    public List<Document> getAllDocument() {
        return libraryServices.getAll();
    }

    @GetMapping("/get")
    public Document getDocumentById(@RequestParam String id) {
        return libraryServices.get(id);
    }

    @GetMapping("/get/{id}")
    public Document getDocumentById2(@PathVariable(name = "id") String id) {
        return libraryServices.get(id);
    }
}
