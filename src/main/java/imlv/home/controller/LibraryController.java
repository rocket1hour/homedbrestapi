package imlv.home.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import imlv.home.entities.Document;
import imlv.home.services.library.LibraryServices;
import imlv.home.services.factory.LibraryServicesFactory;

@RestController
@RequestMapping("/imlv/library")
public class LibraryController {
    
    @Autowired
    private LibraryServicesFactory<LibraryServices> lbraryServicesFactory;

    private LibraryServices libraryServices;

    @GetMapping("/get/{type}")
    public List<Document> getAllDocumentByType(@PathVariable(name = "type") Integer type) {
        libraryServices = (LibraryServices) lbraryServicesFactory.filterServices(type);
        return libraryServices.getAll();
    }

}