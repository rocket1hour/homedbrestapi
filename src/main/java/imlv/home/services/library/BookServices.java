package imlv.home.services.library;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import imlv.home.entities.Document;
import imlv.home.entities.Document.Book;
import java.lang.reflect.Type;
import java.util.List;

@Component("book")
@Primary
public class BookServices extends DocumentServices{
    
    @Override
    public List<Document> getAll() {
        JsonArray jsonArray = getAllCanBplFilterByType(filterType);
        Gson gson = new Gson();
        Type typeOfLst = new TypeToken<List<Document.Book>>() {}.getType();
        List<Document> lstType = gson.fromJson(jsonArray, typeOfLst);
        
        return lstType;
    }

    @Override
    public Document get(String entityId) {
        Gson gson = new Gson();
        JsonObject entity = filterById(entityId);
        Document document = gson.fromJson(entity, Book.class);
        return document;
    }
}
