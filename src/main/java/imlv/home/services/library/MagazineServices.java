package imlv.home.services.library;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.springframework.stereotype.Component;

import imlv.home.entities.Document;
import imlv.home.entities.Document.Magazine;

import java.lang.reflect.Type;
import java.util.List;
import imlv.home.util.Constants;

@Component("magazine")
public class MagazineServices extends DocumentServices{

    @Override
    public List<Document> getAll() {
        filterType = Constants.ENTITY_TYPE_MAGAZINE;
        JsonArray jsonArray = getAllCanBplFilterByType(filterType);
        Gson gson = new Gson();
        Type typeOfLst = new TypeToken<List<Document.Magazine>>() {
        }.getType();
        List<Document> lstType = gson.fromJson(jsonArray, typeOfLst);

        return lstType;
    }

    @Override
    public Document get(String entityId) {
        Gson gson = new Gson();
        JsonObject entity = filterById(entityId);
        Document document = gson.fromJson(entity, Magazine.class);
        return document;
    }
}
