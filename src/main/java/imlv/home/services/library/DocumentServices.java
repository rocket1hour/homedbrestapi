package imlv.home.services.library;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.springframework.stereotype.Service;

import imlv.home.util.Constants;
import imlv.home.util.Util;

@Service
public abstract class DocumentServices implements LibraryServices{
    
    Integer filterType = Constants.ENTITY_TYPE_BOOK;
    String entityID = Constants.ENTIRY_INFO_ID;
    
    public JsonArray getAllDatabase(){
        JsonArray jsonArray = Util.getNewUtil().convertStringToJsonArray("/db/entities/thuvien/documents.json");
        return jsonArray;
    }

    public JsonArray getAllCanBplFilterByInstance(String filter){
        JsonArray jsonArray = getAllDatabase();
        JsonArray results = new JsonArray();
        for (JsonElement jsonElement : jsonArray) {
            JsonObject canBo = jsonElement.getAsJsonObject();
            if(canBo.get(filter).isJsonNull() == false){
                results.add(jsonElement);
            }
        }
        return results;
    }
    
    public JsonArray getAllCanBplFilterByType(Integer filterType){
        JsonArray jsonArray = getAllDatabase();
        JsonArray results = new JsonArray();
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            boolean matchType = (jsonObject.get(Constants.ENTITY_TYPE).isJsonNull() == false) && (jsonObject.get(Constants.ENTITY_TYPE).getAsInt() == filterType);
            if(matchType == true){
                results.add(jsonElement);
            }
        }
        return results;
    }
    
    public JsonObject filterById(String entityId){
        JsonArray jsonArray = getAllCanBplFilterByType(filterType);
        JsonObject jsonObject = null;
        for (JsonElement jsonElement : jsonArray) {
            JsonObject entity = jsonElement.getAsJsonObject();
            boolean hasEntity = (entity.get(entityID).isJsonNull() == false) 
                                && (entity.get(entityID).getAsString().equals(entityId));
            if(hasEntity == true){
                jsonObject = entity;
            }
        }
        if(jsonObject == null || jsonObject.isJsonNull() == true){
            return null;
        }

        return jsonObject;
    }    
}
