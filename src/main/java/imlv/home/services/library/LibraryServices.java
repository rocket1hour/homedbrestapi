package imlv.home.services.library;

import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Service;
import imlv.home.entities.Document;

@Service
public interface LibraryServices {

    public JsonArray getAllDatabase();

    public JsonArray getAllCanBplFilterByInstance(String filter);

    public JsonArray getAllCanBplFilterByType(Integer filterType);

    public JsonObject filterById(String entityId);

    public List<Document> getAll();
    
    public Document get(String entityId);
}