package imlv.home.services.factory;

import org.springframework.stereotype.Component;

@Component
public interface ServicesFactory<T> {
    
    public T getService();

    public T filterServices(Integer type);
}