package imlv.home.services.factory;

import org.springframework.stereotype.Component;

@Component
public interface LibraryServicesFactory<LibraryServices> extends ServicesFactory<LibraryServices>{
    
    @Override
    public LibraryServices getService();

    @Override
    public LibraryServices filterServices(Integer type);

}