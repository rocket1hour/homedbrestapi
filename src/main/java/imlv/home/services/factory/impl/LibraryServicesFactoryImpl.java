package imlv.home.services.factory.impl;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import imlv.home.services.factory.LibraryServicesFactory;
import imlv.home.services.library.BookServices;
import imlv.home.services.library.LibraryServices;
import imlv.home.services.library.MagazineServices;
import imlv.home.services.library.NewspaperServices;

@Component
@Primary
public class LibraryServicesFactoryImpl implements LibraryServicesFactory<LibraryServices> {

    private LibraryServices libraryServices;

    @Override
    public LibraryServices getService() {
        return this.libraryServices;
    }

    @Override
    public LibraryServices filterServices(Integer type) {
        switch (type) {
            case 0:
                break;
            case 1:
                libraryServices = new BookServices();
                break;
            case 2:
                libraryServices = new MagazineServices();
                break;
            case 3:
                libraryServices = new NewspaperServices();
                break;
            default:

        }

        return this.getService();
    }
}